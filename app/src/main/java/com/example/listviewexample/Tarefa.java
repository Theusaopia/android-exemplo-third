package com.example.listviewexample;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Tarefa implements Serializable {
    private String descricao;
    private Date dataTarefa;
    private int duracao;
    private boolean feita;

    public Tarefa() {

    }

    public Tarefa(String descricao, Date dataTarefa, int duracao, boolean feita) {
        this.descricao = descricao;
        this.duracao = duracao;
        this.feita = feita;
        this.dataTarefa = dataTarefa;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    public boolean isFeita() {
        return feita;
    }

    public void setFeita(boolean feita) {
        this.feita = feita;
    }

    public Date getDataTarefa() {
        return dataTarefa;
    }

    public void setDataTarefa(Date dataTarefa) {
        this.dataTarefa = dataTarefa;
    }

    @Override
    public String toString() {
        String pattern = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return descricao + " | " + sdf.format(dataTarefa) + " | " + duracao + " | " + (feita ? "OK" : "A FAZER");
    }
}
