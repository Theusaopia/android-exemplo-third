package com.example.listviewexample;

import androidx.annotation.IdRes;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemLongClickListener,
        AdapterView.OnItemClickListener {
    EditText edTarefa, edDuracao, edDataTarefa, edDataFiltro;
    ListView listaTarefa;
    TarefaAdapter adapter;
    TarefaAdapter tarefaAdaptador = null;

    List<Tarefa> tarefas;
    List<Tarefa> tarefasFiltradas;
    List<Integer> selecionados = new LinkedList<>();

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    Calendar cal = Calendar.getInstance();

    class TarefaAdapter extends ArrayAdapter<Tarefa> {
        List<Tarefa> tasks;
        List<Tarefa> backup = new ArrayList<>();

        public TarefaAdapter(List<Tarefa> tarefas) {
            super(MainActivity.this, R.layout.item_lista_tarefa, tarefas);
            tasks = tarefas;
            backup.addAll(tarefas);
        }

        public void reiniciarLista() {
            tasks.clear();
            tasks.addAll(backup);
        }

        public void alimentarBackup(Tarefa tarefa) {
            backup.add(tarefa);
        }

        public void atualizarLista(List<Tarefa> tarefas) {
            tasks.clear();
            tasks.addAll(tarefas);
        }

        public List<Tarefa> getBackup() {
            return backup;
        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public View getView(int pos, View recicled, ViewGroup parent) {
            if(recicled == null) {
                recicled = getLayoutInflater().inflate(R.layout.item_lista_tarefa, null);
            }

            Tarefa t = tasks.get(pos);
            ((TextView) recicled.findViewById(R.id.il_tarefa)).setText(t.getDescricao());
            ((TextView) recicled.findViewById(R.id.quando)).setText(sdf.format(t.getDataTarefa()));
            ((TextView) recicled.findViewById(R.id.il_duracao)).setText(String.valueOf(t.getDuracao()));
            ((CheckBox) recicled.findViewById(R.id.feita)).setChecked(t.isFeita());

            if(selecionados.contains(pos)) {
                recicled.setBackgroundResource(android.R.color.holo_blue_light);
            }
            else{
                recicled.setBackgroundResource(android.R.color.white);
            }

            return recicled;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null) {
            tarefas = (ArrayList<Tarefa>) savedInstanceState.getSerializable("tarefas");
        }


        if(tarefas == null) {
            tarefas = new ArrayList<>();
            tarefas.add(new Tarefa("Assistir programa do craque neto", new Date(System.currentTimeMillis()), 10, false));
            tarefas.add(new Tarefa("Fazer ovo cozido", new Date(System.currentTimeMillis()), 30, false));
            tarefas.add(new Tarefa("Ouvir forró", new Date(System.currentTimeMillis()), 30, false));
        }

        edTarefa = (EditText) findViewById(R.id.edTarefa);
        edDuracao = (EditText) findViewById(R.id.edDuracao);
        listaTarefa = (ListView) findViewById(R.id.listaTarefas);
        edDataTarefa = (EditText) findViewById(R.id.edDataTarefa);
        edDataFiltro = (EditText) findViewById(R.id.edDataFiltro);
        tarefaAdaptador = new TarefaAdapter(tarefas);
        adapter = tarefaAdaptador;
        //adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_single_choice, tarefas);

        listaTarefa.setAdapter(adapter);
        //listaTarefa.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listaTarefa.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        listaTarefa.setOnItemLongClickListener(this);
        listaTarefa.setOnItemClickListener(this);

        edDataFiltro.setText(calToString(cal));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tarefas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_adicionar) {
            adicionar(item);
        }

        return true;
    }

    public void adicionar(MenuItem item) {
        confirmar(null);
    }

    public void remover(MenuItem item) {
        remover((View) null);
    }

    public void sair(MenuItem item) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setTitle("Confirmar");
        bld.setMessage("Sair do aplicativo sim ou nao");
        bld.setNegativeButton("Não", null);
        bld.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        bld.show();
    }

    @Override
    public void onBackPressed() {
        sair(null);
    }

    @Override
    public void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putSerializable("tarefas", (Serializable) tarefas);
    }

    public void confirmar(View v){
        String tarefaDigitada = edTarefa.getText().toString();
        int duracao = Integer.parseInt(edDuracao.getText().toString());
        Date dataTarefa = null;

        try {
            dataTarefa = sdf.parse(edDataTarefa.getText().toString());
        } catch (ParseException e) {
            Toast.makeText(this, "Data Inválida", Toast.LENGTH_SHORT).show();
        }

        //trim: tira os espaços em branco da String
        if(tarefaDigitada.trim().length() > 0) {
            Tarefa t = new Tarefa(tarefaDigitada, dataTarefa, duracao, false);
            tarefas.add(t);
            tarefaAdaptador.alimentarBackup(t);
            adapter.notifyDataSetChanged(); //atualiza a listview "manualmente"
        }

        edTarefa.setText("");
        edDuracao.setText("");
        edDataTarefa.setText("");
    }

    public void remover(View v) {
        if(listaTarefa.getChoiceMode() == AbsListView.CHOICE_MODE_SINGLE) {
            int pos = listaTarefa.getCheckedItemPosition();

            if (pos == -1) {
                Toast.makeText(this, "Selecione a tarefa!", Toast.LENGTH_SHORT).show();
            } else {
                tarefas.remove(pos);
                adapter.notifyDataSetChanged();
                listaTarefa.clearChoices();
            }
        }
        else{
            if(listaTarefa.getCheckedItemCount() > 0) {
                SparseBooleanArray sels = listaTarefa.getCheckedItemPositions();

                List<Tarefa> aRemover = new LinkedList<>();

                for (int i = 0; i < tarefas.size(); i++) {
                    if(sels.get(i)) {
                        aRemover.add(tarefas.get(i));
                    }
                }

                tarefas.removeAll(aRemover);
                adapter.notifyDataSetChanged();
                listaTarefa.clearChoices();
                selecionados.clear();
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long l) {
        Tarefa t = tarefas.get(pos);
        if(!t.isFeita()){
            t.setFeita(true);
        }
        else{
            t.setFeita(false);
        }

        adapter.notifyDataSetChanged();

        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {

        if(selecionados.contains(pos)) {
            selecionados.remove(new Integer(pos)); //autobox, remove tem implementacao com int e Integer
        }
        else {
            selecionados.add(pos);
        }

        adapter.notifyDataSetChanged();
    }

    public String calToString(Calendar cal) {
        Date date = cal.getTime();
        return sdf.format(date);
    }

    public void voltarUmDia(View v) {
        cal.add(Calendar.DATE, -1);
        edDataFiltro.setText(calToString(cal));
        adapter.notifyDataSetChanged();
    }

    public void avancarUmDia(View v) {
        cal.add(Calendar.DATE, 1);
        edDataFiltro.setText(calToString(cal));
        adapter.notifyDataSetChanged();
    }

    public void atualizarFiltro(View v) throws ParseException {
        Date data = sdf.parse(edDataFiltro.getText().toString());
        String date1 = sdf.format(data);
        tarefasFiltradas = new LinkedList<>();

        for (Tarefa t : tarefaAdaptador.getBackup()) {
            String date2 = sdf.format(t.getDataTarefa());
            if(date1.equals(date2)) {
                tarefasFiltradas.add(t);
            }
        }

        tarefaAdaptador.reiniciarLista();
        tarefaAdaptador.atualizarLista(tarefasFiltradas);
        adapter.notifyDataSetChanged();
    }

    public void resetarFiltro(View view) {
        edDataFiltro.setText(sdf.format(new Date(System.currentTimeMillis())));
        tarefaAdaptador.atualizarLista(tarefaAdaptador.getBackup());
        adapter.notifyDataSetChanged();
    }
}